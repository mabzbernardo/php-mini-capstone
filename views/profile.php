<?php 

require("../partials/template.php");

function get_title(){
	echo "Profile";
};

function get_body_contents(){
require("../controllers/connection.php");
?>

<h1 class="text-center py-3">Profile Page</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<!-- Profile Details -->
			<!-- <div class="card-body">
				<h4 class="card-title text-center py-3">Contacts:</h4>
			</div> -->
		</div>
		<div class="col-lg-6">
			<h1>Addresses:</h1>
			<ul>
				<?php
				$userId = $_SESSION['user']['id'];
					$address_query = "SELECT * FROM addresses WHERE user_id = $userId";
					$addresses = mysqli_query($conn, $address_query);
					foreach($addresses as $indiv_address) {
				?>

					<li>
						<?= 
						$indiv_address['address1'] . "," .
						$indiv_address['address2'] . "<br>". " " .
						$indiv_address['zipcode'];
						// var_dump($indiv_address['zipcode']);
						?>
						
					</li>
				<?php
					}

				 ?>
			</ul>
			<form action="../controllers/add-address-process.php" method="POST">
				<div class="form-group">
					<label for="address1">Address 1:</label>
					<input type="text" name="address1" class="form-control">
				</div>
				<div class="form-group">
					<label for="address2">Address 2:</label>
					<input type="text" name="address2" class="form-control">
				</div>
				<div class="form-group">
					<label for="zipCode">Zip Code:</label>
					<input type="text" name="zipCode" class="form-control">
				</div>
				<div class="form-group">
					<label for="city">City :</label>
					<input type="text" name="city" class="form-control">
				</div>
				<input type="hidden" name="user_id" value="<?= $userId ?>">
				<button class="btn btn-secondary" type="submit">Add Address</button>
			</form>

			<h4>Contacts</h4>
			<ul>
				<?php 
				$userId = $_SESSION['user']['id'];
					$contact_query = "SELECT * FROM contacts WHERE user_id = $userId";
					$contacts = mysqli_query($conn, $contact_query);
					foreach($contacts as $indiv_contact){
					?>
					<li>
						<?php echo $indiv_contact['contactNo']; ?>
					</li>

					<?php
					}
				?>
			</ul>
			<form action="../controllers/add-contact-process.php" method="POST">
				<div class="form-group">
					<label for="contact">Contact Number:</label>
					<input type="number" name="submitcontactNo" class="form-control" value="">
					<input type="hidden" name="contactNo" class="form-control" value="<?= $userId ?>">
					<button class="btn btn-secondary" type="submit">Add Contact</button>
				</div>
			</form>

			
		</div>
	</div>
</div>


<?php
}
 ?>