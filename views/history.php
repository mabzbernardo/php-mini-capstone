<?php 

	require "../partials/template.php";

function get_title(){
		echo "History";
	}

	function get_body_contents(){
	// require connection
	require "../controllers/connection.php";


?>

	<h1>Order History</h1>
	<hr>
	<table>
		<thead>
			<tr>
				<th>Order ID</th>
				<th>Items</th>
				<th>Total</th>	
			</tr>
		</thead>
		<tbody>
			<?php 

				$userId = $_SESSION['user']['id'];
				$order_query = "SELECT * FROM orders WHERE user_id = $userId";
				$orders = mysqli_query($conn, $order_query);

				foreach($orders as $indiv_order){

				?>

					<tr>
						<td><?= $indiv_order['id']?></td>
						<td>
							<?php 

							$orderId = $indiv_order['id'];

								$items_query = "SELECT * FROM items JOIN item_order ON (item.id = item_order.item_id) WHERE item_order.order_id = $orderId";

								$items = mysqli_query($conn, $items_query);
								foreach($items as $indiv_item){
								?>
									<span><?= $indiv_item['name'] ?></span>

								<?php
								}
							?>
						</td>
						<td><?= $indiv_order['total']?></td>
					</tr>
				<?php
				}

			?>
		</tbody>
		</table>
	<?php
	}
	?>